pb:
	@mkdir -p bin
	protoc -I=api --go_out=bin api/*.proto
	 
clean:
	rm bin/*.go

run:
	go run main.go