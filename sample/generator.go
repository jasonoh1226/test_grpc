package sample

import "gitlab.com/jasonoh1226/test_grpc/api"

// NewKeyboard returns a new sample keyboard
func NewKeyboard() *api.Keyboard {
	keyboard := &api.Keyboard{
		Layout:  randomKeyboardLayout(),
		Backlit: randomBool(),
	}
	return keyboard
}

func NewCPU() *api.CPU {
	cpu := &api.CPU{}
	return cpu
}
