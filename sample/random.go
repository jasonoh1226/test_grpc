package sample

import (
	"math/rand"

	"gitlab.com/jasonoh1226/test_grpc/api"
)

func randomKeyboardLayout() api.Keyboard_Layout {
	switch rand.Intn(3) {
	case 1:
		return api.Keyboard_QWERTY
	case 2:
		return api.Keyboard_QWERTZ
	default:
		return api.Keyboard_AZERTY
	}
}

func randomBool() bool {
	return rand.Intn(2) == 1
}
